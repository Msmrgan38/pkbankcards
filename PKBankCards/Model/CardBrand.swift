
//
//  CardBrand.swift
//  PKBankCards
//
//  Created by divya kothagattu on 03/09/20.
//  Copyright © 2020 divya kothagattu. All rights reserved.
//


import Foundation

enum CardBrand {
    case visa
    case mastercard
    case americanExpress
    case maestro
}
